const express = require('express');
const bodyParser = require('body-parser');

//Routing
const register = require('./routes/user.route');
const login = require('./routes/user.route');

//Express
const app = express();

//Body Parser-Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api/library', login);
app.use('/api/library', register);

//PORT
let port = 5000;

app.listen(port, () => {
  console.log('Server is up and running on port number ' + port);
});
