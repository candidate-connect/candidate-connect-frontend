import api from './api';
import setAuthToken from '../setAuthToken';
import jwt_decode from 'jwt-decode';

export function authData(login_data) {
  return {
    type: 'LOGIN',
    payload: api
      .post('login/', login_data)
      .then(res => {
        const { token } = res.data;
        console.log('token===>>', token);
        console.log('setitem===>>', localStorage.setItem('jwtToken', token));
        setAuthToken(token);
        console.log('setAuthToken===>>>', setAuthToken(token));
        // const decoded = jwt_decode(token);
        // dispatch(setCurrentUser(decoded));
        // alert('Successfully Logged In');
      })
      .catch(err => {
        console.log('err');
      })
  };
}

export function logout() {
  return {
    type: 'LOGOUT'
  };
}
