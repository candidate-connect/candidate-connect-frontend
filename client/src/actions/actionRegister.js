import api from './api';

export function registerData(data) {
  return {
    type: 'REGISTER',
    payload: api.post('signup/', data)
  };
}

export function changePwd(data) {
  return {
    type: 'CHANGEPWD',
    payload: api.post('change_password/', data)
  };
}

export function verifyReset() {
  return {
    type: 'VERIFYRESET',
    payload: api.get('verify_reset')
  };
}

export function resetPwd(data) {
  return {
    type: 'RESETPWD',
    payload: api.post('forgot_password/', data)
  };
}
