import React, { Component } from 'react';
import { BrowserRouter as Link } from 'react-router-dom';
import './Sidebar.css';
import { slide as Menu } from 'react-burger-menu';

export default props => {
  return (
    <Menu class>
      <Link className="menu-item" to="/">
        <Link className="menu-item" to="/dashboard">
          <span>
            <i class="fas fa-tachometer-alt"></i>
          </span>
        </Link>
        Dashboard
      </Link>
      <Link className="menu-item" to="/connecttolinkedin">
        <span>
          <i class="fab fa-linkedin"></i>
        </span>
        Connect to LinkedIn
      </Link>
      <Link className="menu-item" to="/searchprofiles">
        <span>
          <i class="far fa-id-badge"></i>
        </span>
        Search Profiles
      </Link>
      <Link className="menu-item" to="/savedprofiles">
        <span>
          <i class="fas fa-user-plus"></i>
        </span>
        Saved Profiles
      </Link>
      <Link className="menu-item" to="/archives">
        <span>
          <i class="fas fa-folder"></i>
        </span>
        Archives
      </Link>
    </Menu>
  );
};
