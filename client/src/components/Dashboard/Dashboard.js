import React, { Component } from 'react';
import './Dashboard.css';
import registerimage from '../../images/register.png';
import loginImage from '../../images/login.png';

class Dashboard extends Component {
  state = {};
  render() {
    return (
      <div className="container">
        <div className="row dashboard-content">
          <div className="col">
            <img src={registerimage} className="img-fluid" alt="Register" />
          </div>
          <div className="col">
            <img src={loginImage} className="img-fluid" alt="login" />
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
