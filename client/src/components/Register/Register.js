import React, { Component } from 'react';
import { Form } from 'reactstrap';
import {
  renderField,
  required,
  renderRadio,
  email,
  password
} from '../../validations/formValidations';
import { Field, reduxForm } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { NotificationManager } from 'react-notifications';
import Proptypes from 'prop-types';
import { registerData } from '../../actions/actionRegister';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import registerimage from '../../images/register.png';
import './Register.css';

const validate = values => {
  const errors = {};
  if (values.password !== values.confirm_password) {
    errors.confirm_password = 'Confirm Password should match Password';
  }
  return errors;
};

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: null
    };
  }

  handleCreateSuccess() {
    NotificationManager.success(
      'User Registered Successfully',
      'Register Successfully'
    );
    this.props.history.push('/login');
  }

  handleCreateError() {
    const { isError } = this.props;
    if (isError) {
      NotificationManager.error(
        'User Registered  Failed',
        'Please try again',
        3000
      );
    }
    NotificationManager.error('Connection Refused');
  }

  submitUser = ({
    first_name,
    last_name,
    gender,
    email,
    role,
    password,
    confirm_password
  }) => {
    const userData = new FormData();
    userData.append('first_name', first_name);
    userData.append('last_name', last_name);
    userData.append('gender', gender);
    userData.append('email', email);
    userData.append('role', role);
    userData.append('password', password);
    userData.append('confirm_password', confirm_password);
    this.props
      .registerData(userData)
      .then(() => this.handleCreateSuccess())
      .catch(() => this.handleCreateError());
  };

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-7 img-register">
            <img src={registerimage} className="img-fluid" alt="Register" />
          </div>
          <div className="col-md-5 img-register">
            <div className="registerForm">
              <h3 className="mb-4">Register</h3>
              <Form onSubmit={handleSubmit(this.submitUser)}>
                <div className="input-group mb-4">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-user"></i>
                    </span>
                  </div>
                  <Field
                    type="text"
                    name="first_name"
                    id="first_name"
                    component={renderField}
                    validate={required}
                    placeholder="First Name"
                  />
                </div>
                <div className="input-group mb-4">
                  <div className="input-group-prepend"></div>
                  <span className="input-group-text">
                    <i className="fas fa-user"></i>
                  </span>
                  <Field
                    type="text"
                    name="last_name"
                    id="last_name"
                    component={renderField}
                    validate={required}
                    placeholder="Last Name"
                  />
                </div>
                <div className="input-group mb-4">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-envelope"></i>
                    </span>
                  </div>
                  <Field
                    type="email"
                    name="email"
                    id="email"
                    component={renderField}
                    validate={[required, email]}
                    placeholder="Email"
                  />
                </div>
                <div className="input-group mb-4">
                  <span>Gender</span>
                  <label className="gender">
                    <Field
                      name="gender"
                      id="gender"
                      type="radio"
                      component={renderRadio}
                      validate={required}
                      value="M"
                    />{' '}
                    Male
                  </label>
                  <label className="gender">
                    <Field
                      name="gender"
                      id="gender"
                      component={renderRadio}
                      validate={required}
                      type="radio"
                      value="F"
                    />{' '}
                    Female
                  </label>
                </div>
                <div className="input-group mb-4">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-key"></i>
                    </span>
                  </div>
                  <Field
                    type="password"
                    name="password"
                    id="password"
                    component={renderField}
                    validate={[required, password]}
                    placeholder="Password"
                  />
                </div>
                <div className="input-group mb-4">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-key"></i>
                    </span>
                  </div>
                  <Field
                    type="password"
                    name="confirm_password"
                    id="confirm_password"
                    component={renderField}
                    validate={[required, password]}
                    placeholder="Confirm Password"
                  />
                </div>
                <div className="input-group mb-4">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-user"></i>
                    </span>
                  </div>
                  <div>
                    <Field
                      name="role"
                      validate={required}
                      component="select"
                      className="dropdowntext"
                    >
                      <option value="manager">Manager</option>
                      <option value="employee">Employee</option>
                    </Field>
                  </div>
                </div>
                <p>
                  Have account? <Link to="/login">Login</Link>
                </p>
                <button className="btn btn-primary">Register</button>
                <Link to="/" className="btn btn-primary">
                  Cancel
                </Link>
              </Form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Register.propTypes = {};

let registerWithRouter = withRouter(Register);

let registerForm = connect(
  state => ({
    isError: state.register.isError,
    isLoading: state.register.isLoading
  }),
  dispatch => ({
    registerData: userData => dispatch(registerData(userData))
  })
)(registerWithRouter);

export default reduxForm({
  form: 'registerForm',
  validate
})(registerForm);
