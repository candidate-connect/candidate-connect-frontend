import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import loginImage from '../../images/login.png';
import { Field, reduxForm } from 'redux-form';
import { authData } from '../../actions/actionLogin';
import { connect } from 'react-redux';
import { required, renderField } from '../../validations/formValidations';
import { withRouter } from 'react-router-dom';
import { NotificationManager } from 'react-notifications';
import Proptypes from 'prop-types';
import './Login.css';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  handleCreateSuccess() {
    NotificationManager.success(
      'User login Successfully',
      'Login Successfully'
    );
    this.props.history.push('/dashboard');
  }

  handleCreateError() {
    const { isError } = this.props;
    if (isError) {
      NotificationManager.error(isError, 'Post', 3000);
    }
  }

  handleLogin = ({ email, password }) => {
    const login_data = new FormData();
    login_data.append('email', email);
    login_data.append('password', password);
    this.props
      .authData(login_data)
      .then(() => this.handleCreateSuccess())
      .catch(() => this.handleCreateError());
  };

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="container">
        <div className="row login-content">
          <div className="col-md-7">
            <img src={loginImage} className="img-fluid" alt="Login" />
          </div>
          <div className="col-md-5">
            <div className="loginForm">
              <p>
                To keep connected with us please login with your personal
                information by email address and password.
              </p>
              <form onSubmit={handleSubmit(this.handleLogin)}>
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-envelope"></i>
                    </span>
                  </div>
                  <Field
                    type="email"
                    placeholder="Email Id"
                    name="email"
                    id="email"
                    validate={required}
                    component={renderField}
                  />
                </div>
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fas fa-key"></i>
                    </span>
                  </div>
                  <Field
                    name="password"
                    placeholder="Password"
                    component={renderField}
                    type="password"
                    id="password"
                    validate={required}
                  />
                </div>
                <p>
                  <input type="checkbox" />
                  <span className="rememberPassword ml-2">
                    Remember Password
                  </span>
                </p>
                <p className="forgotLinks">
                  <Link to="/" className="mr-3">
                    Forgot Password
                  </Link>
                  <Link to="/signup">Sign Up</Link>
                </p>
                <Button type="submit" className="btn btn-primary">
                  Login
                </Button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {};

let LoginWithRouter = withRouter(Login);

let LoginForm = connect(
  state => ({
    user: state.auth.user,
    isError: state.auth.isError,
    isLoading: state.auth.isLoading
  }),
  dispatch => ({
    authData: login_data => dispatch(authData(login_data))
  })
)(LoginWithRouter);

export default reduxForm({
  form: 'LoginPageForm'
})(LoginForm);
