import React, { Component } from 'react';
import homePageImage from '../../../images/candidateconn.png';
import { Link } from 'react-router-dom';

import './Homepage.css';

class Homepage extends Component {
  render() {
    return (
      <div className="container">
        <div className="row home-content">
          <div className="col-md-6">
            <div className="home-left-content mb-3">
              <h3>THE ART OF HIRING</h3>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam aut
                ut sint ipsa deleniti fuga ea, a non pariatur aperiam voluptas
                porro quibusdam, ducimus corrupti eaque? Tempora obcaecati
                recusandae sapiente! Lorem ipsum dolor sit amet consectetur
                adipisicing elit. Nam aut ut sint ipsa deleniti fuga ea, a non
                pariatur aperiam voluptas porro quibusdam, ducimus corrupti
                eaque? Tempora obcaecati recusandae sapiente! Lorem ipsum dolor
                sit amet consectetur adipisicing elit. Nam aut ut sint ipsa
                deleniti fuga ea, a non pariatur aperiam voluptas porro
                quibusdam, ducimus corrupti eaque? Tempora obcaecati recusandae
                sapiente!
              </p>
              <Link className="btn btn-primary" to="/aboutus">
                About Us
              </Link>
              <Link className="btn btn-primary" to="/login">
                Login
              </Link>
            </div>
          </div>
          <div className="col-md-6">
            <img
              src={homePageImage}
              className="img-fluid HomepageImage"
              alt="HomepageImage"
            />
          </div>
        </div>
      </div>
    );
  }
}
export default Homepage;
