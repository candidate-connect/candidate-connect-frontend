import React, { Component } from 'react';
import { BrowserRouter as Link } from 'react-router-dom';

import './Footer.css';

class Footer extends Component {
  render() {
    return (
      <footer>
        <div className="container-fluid footer-heading">
          <div className="row">
            <div className="col-md-6">
              <p>
                A product of Spanidea
                <span className="d-block">Pune 7799885544</span>
              </p>
              <p>Copyright 2020 SpanIdea Systems Pvt Ltd.</p>
            </div>
            <div className="col-md-6">
              <ul className="float-md-right">
                <li>
                  <Link to="https://facebook.com/login">
                    <i className="fab fa-facebook-f"></i>
                  </Link>
                </li>
                <li>
                  <Link to="https://twitter.com/login?lang=en">
                    <i className="fab fa-twitter"></i>
                  </Link>
                </li>

                <li>
                  <Link to="https://www.linkedin.com/">
                    <i className="fab fa-linkedin"></i>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
