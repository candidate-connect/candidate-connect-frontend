import React, { Component } from 'react';
import whoweare from '../../images/whoweare.png';
import { Link } from 'react-router-dom';
import './Aboutus.css';

class Aboutus extends Component {
  render() {
    return (
      <div className="container">
        <div className="row aboutus-content">
          <div className="col-md-6 img-about">
            <img src={whoweare} className="img-fluid" alt="HomepageImage" />
          </div>
          <div className="col-md-6">
            <div className="about-left-content mb-3">
              <h3>WHO WE ARE!</h3>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam aut
                ut sint ipsa deleniti fuga ea, a non pariatur aperiam voluptas
                porro quibusdam, ducimus corrupti eaque? Tempora obcaecati
                recusandae sapiente! Lorem ipsum dolor sit amet consectetur
                adipisicing elit. Nam aut ut sint ipsa deleniti fuga ea, a non
                pariatur aperiam voluptas porro quibusdam, ducimus corrupti
                eaque? Tempora obcaecati recusandae sapiente! Lorem ipsum dolor
                sit amet consectetur adipisicing elit. Nam aut ut sint ipsa
                deleniti fuga ea, a non pariatur aperiam voluptas porro
                quibusdam, ducimus corrupti eaque? Tempora obcaecati recusandae
                sapiente!
              </p>
              <Link className="btn btn-primary" to="/aboutus">
                About Us
              </Link>
              <Link className="btn btn-primary" to="/login">
                Login
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Aboutus;
