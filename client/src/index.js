import React, { Suspense, lazy } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import configureStore from './store';
import jwt_decode from 'jwt-decode';
import setAuthToken from './setAuthToken';
// import { setCurrentUser, logoutUser } from './actions/authentication';

import Navbar from './components/Layout/Navbar/Navbar';
import Footer from './components/Layout/Footer/Footer';
import Homepage from './components/Layout/Homepage/Homepage';
import Aboutus from './components/Aboutus/Aboutus';
import Register from './components/Register/Register';
import Login from './components/Login/Login';
import { NotificationContainer } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import Dashboard from './components/Dashboard/Dashboard';
// import Sidebar from './components/Sidebar/Sidebar';

import * as serviceWorker from './serviceWorker';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Suspense fallback={'Loading...'}>
        <Navbar />
        <NotificationContainer />
        {/* <Sidebar /> */}
        <Route exact path="/" component={Homepage} />
        <Route path="/aboutus" component={Aboutus} />
        <Route path="/signup" component={Register} />
        <Route path="/login" component={Login} />
        <Route path="/dashboard" component={Dashboard} />
        <Footer />
      </Suspense>
    </Router>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
