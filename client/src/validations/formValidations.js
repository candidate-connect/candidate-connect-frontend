import React from 'react';
import _ from 'lodash';
import Proptypes from 'prop-types';
import './formValidation.css';
import Select from 'react-select';

// objects
const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const UUID = /^[0-9]{8}-[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{12}$/i;

const PASSWORD = /^(?=.{8,})/;

const MOBILE = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

const NUMBER = /^[0-9]\d*$/;

const required = value => (value ? undefined : 'Required');

const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined;

const number = value => (NUMBER.test(value) ? undefined : 'Must be a Number');

const minValue = min => value =>
  value && value < min ? `Must be at least ${min}` : undefined;

const maxValue = max => value =>
  value && value > max ? `Must be at least ${max}` : undefined;

const email = value =>
  EMAIL_REGEX.test(value) ? undefined : 'Email is invalid';

const uuid = value => (UUID.test(value) ? undefined : 'UUID Id is Invalid');

const mobile = value =>
  MOBILE.test(value) ? undefined : 'Mobile Number is Invalid';

const password = value =>
  PASSWORD.test(value)
    ? undefined
    : 'Passwords must be at least 8 characters long';

const mustMatch = field => (value, allValues) => {
  const data = allValues.toJS();
  return !_.isEmpty(data) && data[field] === value
    ? undefined
    : 'Fields Must Match';
};

const errorToFields = (data = {}) => {
  const errors = {};
  if (data) {
    _.forEach(data.errors, error => {
      const key = error.source.pointer.replace('/data/attributes/', '');
      const fieldName = _.upperFirst(key.replace('_', ' '));
      errors[key] = `${fieldName} ${error.detail}`;
    });
  }

  return errors;
};

const renderTextArea = ({
  input,
  type,
  placeholder,
  validationError,
  meta: { touched, error, warning }
}) => {
  return (
    <div>
      <div>
        <textarea
          {...input}
          placeholder={placeholder}
          type={type}
          rows="10"
          className={
            validationError || (touched && error)
              ? 'form-control validationError'
              : 'form-control'
          }
        />
      </div>
      <div>
        {touched &&
          ((error && <span style={{ color: 'red' }}>{error}</span>) ||
            (warning && <span style={{ color: 'red' }}>{warning}</span>))}
      </div>
      {validationError && (
        <div>
          {validationError && (
            <span style={{ color: 'red' }}>{validationError.error}</span>
          )}
        </div>
      )}
    </div>
  );
};

const renderField = ({
  input,
  type,
  placeholder,
  validationError,
  meta: { touched, error, warning }
}) => {
  return (
    <div className="relative">
      <div>
        <input
          {...input}
          placeholder={placeholder}
          type={type}
          className={
            validationError || (touched && error)
              ? 'form-control validationError'
              : 'form-control'
          }
        />
      </div>
      <div>
        {touched &&
          ((error && <span className="error_txt">{error}</span>) ||
            (warning && <span className="error_txt">{warning}</span>))}
      </div>
      {validationError && (
        <div>
          {validationError && (
            <span className="error_txt">{validationError}</span>
          )}
        </div>
      )}
    </div>
  );
};

const renderCheckbox = ({
  input,
  checked,
  validationError,
  meta: { touched, error, warning }
}) => {
  return (
    <span>
      <input
        {...input}
        type="checkbox"
        defaultChecked={checked === true ? checked : false}
      />
      <div>
        {touched &&
          ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
      {validationError && (
        <div>{validationError && <span>{validationError}</span>}</div>
      )}
    </span>
  );
};

const reduxFormSelect = props => (
  <Select
    {...props}
    value={props.input.value}
    onChange={value => props.input.onChange(value)}
    onBlur={() => props.input.onBlur(props.input.value)}
    options={props.options.map(val => {
      return val.value;
    })}
  >
    {console.log('111', props.input.value)}
  </Select>
);

const renderRadio = ({ input }) => {
  return (
    <span>
      <input {...input} type="radio" />
    </span>
  );
};

renderField.propTypes = {
  input: Proptypes.object,
  type: Proptypes.string,
  placeholder: Proptypes.string,
  validationError: Proptypes.object,
  meta: Proptypes.object
};

renderTextArea.propTypes = {
  input: Proptypes.object,
  type: Proptypes.string,
  placeholder: Proptypes.string,
  validationError: Proptypes.object,
  meta: Proptypes.object
};

renderCheckbox.propTypes = {
  input: Proptypes.object,
  checked: Proptypes.bool,
  validationError: Proptypes.object,
  meta: Proptypes.object
};

export {
  errorToFields,
  maxValue,
  maxLength,
  minValue,
  number,
  required,
  email,
  mobile,
  mustMatch,
  renderTextArea,
  renderField,
  reduxFormSelect,
  renderCheckbox,
  renderRadio,
  password,
  uuid
};
