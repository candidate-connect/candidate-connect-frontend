export default function(state = [], action = {}) {
  switch (action.type) {
    case 'REGISTER_LOADING':
      return {
        ...state,
        isLoading: true,
        isSuccess: false,
        isError: false
      };
    case 'REGISTER_SUCCESS':
      debugger;
      return {
        ...state,
        isLoading: false,
        isSuccess: true,
        isError: false,
        message: 'Registration Successfully'
      };
    case 'REGISTER_ERROR':
      return {
        ...state,
        isLoading: false,
        isSuccess: false,
        isError: true,
        message: action.payload.response.data.message
      };

    case 'CHANGEPWD_LOADING':
      return {
        ...state,
        isLoading: true,
        isSuccess: false,
        isError: false
      };
    case 'CHANGEPWD_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isSuccess: true,
        isError: false
      };
    case 'CHANGEPWD_ERROR':
      return {
        ...state,
        isLoading: false,
        isSuccess: false,
        isError: true,
        message: action.payload.response.data.message
      };

    case 'VERIFYRESET_LOADING':
      return {
        ...state,
        isLoading: true,
        isSuccess: false,
        isError: false
      };
    case 'VERIFYRESET_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isSuccess: true,
        isError: false
      };
    case 'VERIFYRESET_ERROR':
      return {
        ...state,
        isLoading: false,
        isSuccess: false,
        isError: true,
        message: action.payload.response.data.message
      };

    case 'RESETPWD_LOADING':
      return {
        ...state,
        isLoading: true,
        isSuccess: false,
        isError: false
      };
    case 'RESETPWD_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isSuccess: true,
        isError: false
      };
    case 'RESETPWD_ERROR':
      return {
        ...state,
        isLoading: false,
        isSuccess: false,
        isError: true,
        message: action.payload.response.data.message
      };

    default:
      return {
        ...state
      };
  }
}
