import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import authReducer from './authReducer';
import registerReducer from './registerReducer';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
  routing: routerReducer,
  auth: authReducer,
  register: registerReducer,
  form: formReducer
});

export default rootReducer;
