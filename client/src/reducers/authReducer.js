export default function(state = [], action = {}) {
  switch (action.type) {
    case 'LOGIN_LOADING':
      return {
        ...state,
        isLoading: true,
        isSuccess: false,
        isError: false
      };
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        user: action.payload.response,
        isLoading: false,
        isSuccess: true,
        isError: false
      };

    case 'LOGIN_ERROR':
      return {
        ...state,
        isLoading: false,
        isSuccess: false,
        isError: true,
        message: action.payload.response.data.message
      };

    // case 'LOGOUT':
    //   window.localStorage.clear();
    //   location.reload();
    //   return {
    //     ...state,
    //     isLoading: false,
    //     isSuccess: false,
    //     isError: null
    //   };

    default:
      return {
        ...state
      };
  }
}
